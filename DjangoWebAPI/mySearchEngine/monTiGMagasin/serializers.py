from rest_framework.serializers import ModelSerializer, DateTimeField
from monTiGMagasin.models import InfoProduct
from monTiGMagasin.models import Transaction

class InfoProductSerializer(ModelSerializer):
    class Meta:
        model = InfoProduct
        fields = ('id', 'tig_id', 'name', 'category', 'price', 'unit', 'availability', 'sale', 'discount', 'comments', 'owner', 'quantityInStock')


class TransactionSerializer(ModelSerializer):
    date = DateTimeField()

    class Meta:
      model = Transaction
      fields = ("id", "tigID", "price", "quantity", "type", "date")
