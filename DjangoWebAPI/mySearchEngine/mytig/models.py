from django.db import models
from enum import IntEnum


class FishPriority(models.IntegerChoices):
    POISSONS = 0, 'poissons'
    CRUSTACES = 1, 'crustacés'
    COQUILLAGES = 2, 'coquillages'


class Produit(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    tigID = models.IntegerField(default='-1')
    category = models.IntegerField(choices=FishPriority.choices, default=FishPriority.POISSONS)

    class Meta:
        ordering = ('tigID',)


# Create your models here.
class ProduitEnPromotion(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    tigID = models.IntegerField(default='-1')

    class Meta:
        ordering = ('tigID',)


class ProduitDisponible(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    tigID = models.IntegerField(default='-1')

    class Meta:
        ordering = ('tigID',)
