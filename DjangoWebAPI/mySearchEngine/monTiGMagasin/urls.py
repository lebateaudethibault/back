from django.urls import path
from monTiGMagasin import views

urlpatterns = [
    path('infoproducts/', views.InfoProductList.as_view()),
    path('infoproducts/<str:category>', views.InfoProductListByCategory.as_view()),
    path('infoproduct/<int:tig_id>/', views.InfoProductDetail.as_view()),
    path("putonsale/<int:id>/<str:newprice>/", views.RedirectionMiseEnPromoManuelle.as_view()),
    path("removesale/<int:id>/", views.RedirectionSuppressionPromoManuelle.as_view()),
    path("incrementstock/<int:id>/<int:number>/", views.RedirectionAugmentationStock.as_view()),
    path("decrementstock/<int:id>/<int:number>/", views.RedirectionDiminutionStock.as_view()),
    path("transaction/<int:tigID>/<str:type>/<str:price>/<str:quantity>/", views.AddTransaction.as_view()),
    path("transactions/", views.TransactionList.as_view()),
    path("transaction/getannee/<int:annee>/<str:type>/", views.TransactionAnnee.as_view()),
    path("transaction/gettrimestre/<int:annee>/<int:trimestre>/", views.TransactionTrimestre.as_view()),
    path("transaction/getmois/<int:annee>/<int:mois>/", views.TransactionMois.as_view()),
    path("transaction/getjour/<int:annee>/<int:mois>/<int:jour>/<str:type>/", views.TransactionJour.as_view())
]
