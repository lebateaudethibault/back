from django.core.management.base import BaseCommand, CommandError
from monTiGMagasin.models import Transaction

import time

class Command(BaseCommand):
    help = 'Refresh the list of products from TiG server.'

    def handle(self, *args, **options):
        self.stdout.write('['+time.ctime()+'] Refreshing data...')
        Transaction.objects.all().delete()
        self.stdout.write('['+time.ctime()+'] Data refresh terminated.')
