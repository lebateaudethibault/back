# Back 
- Install the virtual environment (venv) `python3 -m venv myTidyVEnv`
- Activate venv `source myTidyVEnv/bin/activate` (`.\myTidyVEnv\Scripts\activate` on Windows)
- Install django and djangoRestFramework `pip3 install django djangorestframework requests`
- Migrate the database `python3 manage.py makemigrations` <FileName> `python3 manage.py migrate`