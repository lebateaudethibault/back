from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
#from monTiGMagasin.config import baseUrl
from monTiGMagasin.models import InfoProduct, Transaction
from monTiGMagasin.serializers import InfoProductSerializer, TransactionSerializer


# Create your views here.
class InfoProductList(APIView):
    def get(self, request, format=None):
        products = InfoProduct.objects.all()
        serializer = InfoProductSerializer(products, many=True)
        return Response(serializer.data)


# Create your views here.
class InfoProductListByCategory(APIView):
    def get(self, request, category, format=None):
        categorie_mapping = {
            'all': -1,
            'poissons': 0,
            'crustaces': 1,
            'coquillages': 2,
        }

        category = category.lower()

        if category not in categorie_mapping:
            return Response({"error": "Catégorie non valide."}, status=status.HTTP_400_BAD_REQUEST)

        categorie_enum = categorie_mapping[category]

        if categorie_enum < 0:
            products = InfoProduct.objects.all()
        else:
            products = InfoProduct.objects.filter(category=categorie_enum)

        serializer = InfoProductSerializer(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class InfoProductDetail(APIView):
    def get_object(self, tig_id):
        try:
            return InfoProduct.objects.get(tig_id=tig_id)
        except InfoProduct.DoesNotExist:
            raise Http404
    def get(self, request, tig_id, format=None):
        product = self.get_object(tig_id=tig_id)
        serializer = InfoProductSerializer(product)
        return Response(serializer.data)
class RedirectionAugmentationStock(APIView):
    """ /incrementstock/int:id/int:number/ """

    def get_object(self, id):
        try:
            return InfoProduct.objects.get(id = id)
        except InfoProduct.DoesNotExist:
            raise Http404

    def get(self, request, id, number, format = None):
        produit = self.get_object(id)
        InfoProduct.objects.filter(id = id).update(
            quantityInStock = produit.quantityInStock + number)
        produit.refresh_from_db()
        serializer = InfoProductSerializer(produit)
        return Response(serializer.data)

class RedirectionDiminutionStock(APIView):
    """ /decrementstock/int:id/int:number/ """

    def get_object(self, id):
        try:
            return InfoProduct.objects.get(id = id)
        except Exception:
            raise Http404

    def get(self, request, id, number, format = None):
        produit = self.get_object(id)

        # Parti pris de dire qu'un nombre supérieur à quantityInStock revient à
        # supprimer l'entièreté du stock
        if number > produit.quantityInStock:
            InfoProduct.objects.filter(id = id).update(quantityInStock = 0)
        else:
            InfoProduct.objects.filter(id = id).update(
                quantityInStock = produit.quantityInStock - number)
            produit.refresh_from_db()

        serializer = InfoProductSerializer(produit)
        return Response(serializer.data)

class RedirectionMiseEnPromoManuelle(APIView):
    """ /putonsale/int:id/str:newprice/ """

    def get_object(self, id):
        """ Tente la récupération d'un produit en promotion,
        renvoie None si le produit n'existe pas. """
        try:
            return InfoProduct.objects.get(id = id)
        except InfoProduct.DoesNotExist:
            raise Http404

    def get(self, request, id, newprice, format = None):
        try:
            InfoProduct.objects.filter(id = id).update(
                sale = True, discount = float(newprice))
        except Exception:
            return Response({'message': 'newprice doit être un flottant.'})

        produit = self.get_object(id)
        serializer = InfoProductSerializer(produit)

        #promoProduit = ProduitEnPromotion(id = id)
        #promoProduit.save()

        return Response(serializer.data)

class RedirectionSuppressionPromoManuelle(APIView):
    """ /removesale/int:id/ """

    def get_object(self, id):
        try:
            return InfoProduct.objects.get(id = id)
        except InfoProduct.DoesNotExist:
            raise Http404

    def get(self, request, id, format = None):
        # Si un produit existe dans ProduitEnPromotion, on le supprime
        #ProduitEnPromotion.objects.filter(id = id).delete()
        InfoProduct.objects.filter(id = id).update(
            sale = False, discount = 0.0)

        produit = self.get_object(id)
        serializer = InfoProductSerializer(produit)
        return Response(serializer.data)


class AddTransaction(APIView):
    def get(self, request, tigID, type, price, quantity, format = None):
      try:
        transac = Transaction(
          tigID = tigID, type = type, price = float(price), quantity = int(quantity))
        transac.save()
      except Exception:
        return Response({"message": "price doit être un flottant"})

      serializer = TransactionSerializer(transac)
      return Response(serializer.data)


class TransactionList(APIView):
  def get(self, request, format=None):
    transactions = Transaction.objects.all()
    serializer = TransactionSerializer(transactions, many=True)
    return Response(serializer.data)


class TransactionAnnee(APIView):
  def get_object(self, annee, type):
    try:
      return Transaction.objects.filter(
        date__year = annee, type = type)
    except Transaction.DoesNotExist:
      raise Http404

  def get(self, request, annee, type, format = None):
    transac = self.get_object(annee, type)
    serializer = TransactionSerializer(transac, many = True)
    return Response(serializer.data)

class TransactionTrimestre(APIView):
  def get_object(self, annee, trimestre):
    try:
      return Transaction.objects.filter(
        date__year = annee, date__quarter = trimestre, type = "vente")
    except Transaction.DoesNotExist:
      raise Http404

  def get(self, request, annee, trimestre, format = None):
    transac = self.get_object(annee, trimestre)
    serializer = TransactionSerializer(transac, many = True)
    return Response(serializer.data)

class TransactionMois(APIView):
  def get_object(self, annee, mois):
    try:
      return Transaction.objects.filter(
        date__year = annee, date__month = mois, type = "vente")
    except Transaction.DoesNotExist:
      raise Http404

  def get(self, request, annee, mois, format = None):
    transac = self.get_object(annee, mois)
    serializer = TransactionSerializer(transac, many = True)
    return Response(serializer.data)

class TransactionJour(APIView):
  def get_object(self, annee, mois, jour, type):
    try:
      return Transaction.objects.filter(
        date__year = annee, date__month = mois, date__day = jour, type = type)
    except Transaction.DoesNotExist:
      raise Http404

  def get(self, request, annee, mois, jour, type, format = None):
    transac = self.get_object(annee, mois, jour, type)
    serializer = TransactionSerializer(transac, many = True)
    return Response(serializer.data)


