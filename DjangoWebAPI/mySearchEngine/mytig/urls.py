from django.urls import path
from mytig import views

urlpatterns = [
    path('products/', views.RedirectionListeDeProduits.as_view()),
    path('product/<int:pk>/', views.RedirectionDetailProduit.as_view()),
    path('onsaleproducts/', views.PromoList.as_view()),
    path('onsaleproduct/<int:pk>/', views.PromoDetail.as_view()),
    path("shipPoints/", views.PointDeLivraisonList.as_view()),
    path("shipPoint/<int:pk>/", views.PointDeLivraisonDetail.as_view()),
    path("availableproducts/", views.ProduitsDisponibleList.as_view()),
    path("availableproduct/<int:pk>/", views.ProduitsDisponibleDetail.as_view()),
    path('products/<str:category>/', views.ProduitsParCategorie.as_view()),
]
