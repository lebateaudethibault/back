from rest_framework.serializers import ModelSerializer
from mytig.models import ProduitDisponible, ProduitEnPromotion, Produit


class ProduitSerializer(ModelSerializer):
    class Meta:
        model = Produit
        fields = ('id', 'tigID', 'category')


class ProduitEnPromotionSerializer(ModelSerializer):
    class Meta:
        model = ProduitEnPromotion
        fields = ('id', 'tigID')


class ProduitDisponibleSerializer(ModelSerializer):
    class Meta:
        model = ProduitDisponible
        fields = ('id', 'tigID')
