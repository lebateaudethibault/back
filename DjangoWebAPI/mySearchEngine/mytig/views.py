import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from mytig.config import baseUrl


# Create your views here.
class RedirectionListeDeProduits(APIView):
    def get(self, request, format=None):
        res = []
        for prod in Produit.objects.all():
            serializer = ProduitSerializer(prod)
            response = requests.get(baseUrl + 'product/' + str(serializer.data['tigID']) + '/')
            jsondata = response.json()
            res.append(jsondata)
        return JsonResponse(res, safe=False)


#    def post(self, request, format=None):
#        NO DEFITION of post --> server will return "405 NOT ALLOWED"

class RedirectionDetailProduit(APIView):
    def get(self, request, pk, format=None):
        try:
            response = requests.get(baseUrl + 'product/' + str(pk) + '/')
            jsondata = response.json()
            return Response(jsondata)
        except:
            raise Http404


#    def put(self, request, pk, format=None):
#        NO DEFITION of put --> server will return "405 NOT ALLOWED"
#    def delete(self, request, pk, format=None):
#        NO DEFITION of delete --> server will return "405 NOT ALLOWED"


from mytig.models import ProduitDisponible, ProduitEnPromotion, Produit
from mytig.serializers import ProduitDisponibleSerializer, ProduitEnPromotionSerializer, ProduitSerializer
from django.http import Http404
from django.http import JsonResponse


class PromoList(APIView):
    def get(self, request, format=None):
        res = []
        for prod in ProduitEnPromotion.objects.all():
            serializer = ProduitEnPromotionSerializer(prod)
            response = requests.get(baseUrl + 'product/' + str(serializer.data['tigID']) + '/')
            jsondata = response.json()
            res.append(jsondata)
        return JsonResponse(res, safe=False)


#    def post(self, request, format=None):
#        NO DEFITION of post --> server will return "405 NOT ALLOWED"

class PromoDetail(APIView):
    def get_object(self, pk):
        try:
            return ProduitEnPromotion.objects.get(tigID=pk)
        except ProduitEnPromotion.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        prod = self.get_object(pk)
        serializer = ProduitEnPromotionSerializer(prod)
        response = requests.get(baseUrl + 'product/' + str(serializer.data['tigID']) + '/')
        jsondata = response.json()
        return Response(jsondata)


#    def put(self, request, pk, format=None):
#        NO DEFITION of put --> server will return "405 NOT ALLOWED"
#    def delete(self, request, pk, format=None):
#        NO DEFITION of delete --> server will return "405 NOT ALLOWED"

class PointDeLivraisonList(APIView):
    def get(self, request, format=None):
        response = requests.get(baseUrl + "shipPoints/")
        jsondata = response.json()
        return Response(jsondata)


class PointDeLivraisonDetail(APIView):
    def get(self, request, pk, format=None):
        response = requests.get(baseUrl + "shipPoint/" + str(pk) + "/")
        jsondata = response.json()
        return Response(jsondata)


class ProduitsDisponibleList(APIView):
    def get(self, request, format=None):
        res = []

        for prod in ProduitDisponible.objects.all():
            serializer = ProduitDisponibleSerializer(prod)
            response = requests.get(baseUrl + "product/" + str(serializer.data["tigID"]) + "/")
            jsondata = response.json()
            res.append(jsondata)

        return JsonResponse(res, safe=False)


class ProduitsDisponibleDetail(APIView):
    def get_object(self, pk):
        try:
            return ProduitDisponible.objects.get(tigID=pk)
        except:
            raise Http404

    def get(self, request, pk, format=None):
        prod = self.get_object(pk)
        serializer = ProduitDisponibleSerializer(prod)
        response = requests.get(baseUrl + "product/" + str(serializer.data["tigID"]) + "/")
        jsondata = response.json()
        return Response(jsondata)


class ProduitsParCategorie(APIView):
    def get(self, request, category, format=None):

        categorie_mapping = {
            'poissons': 0,
            'crustaces': 1,
            'coquillages': 2,
        }

        categorie_enum = categorie_mapping.get(category.lower())

        if categorie_enum is not None:
            res = []
            for prod in Produit.objects.filter(category = int(categorie_enum)):
                serializer = ProduitSerializer(prod)
                response = requests.get(baseUrl + 'product/' + str(serializer.data['tigID']) + '/')
                jsondata = response.json()
                res.append(jsondata)
            return JsonResponse(res, safe=False)
        else:
            return Response({"error": "Catégorie invalide"}, status=400)
